# VCDL

The is the code for "Variational Collaborative Deep Learning for Soical Recommendation" (under review). It consists of two parts: a Matlab component and a Python component. 

Requirements:

    Python 3.4
    Tensorflow 1.7
    Matlab